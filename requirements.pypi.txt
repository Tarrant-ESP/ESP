#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#                    Electronic Support for Public Health
#                              PIP Requirements
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#-------------------------------------------------------------------------------
#
# Packages from PyPI
#
#-------------------------------------------------------------------------------


psycopg2 >= 2.4.1  # Database Adapter (newer versions have bug with Django test runner -JM 1 Feb 2012)
django == 1.8      # Web application framework with ORM
python-dateutil    # Date-math utilities
sqlparse           # SQL pretty-printer
hl7                # Utilities for working with HL7 message
django_tables2==1.2     # Utilities for working with HTML tables in Django
configobj          # Advanced configuration file support
futures            # Python implementation of "futures" threading paradigm
paramiko           # SSH2 protocol library
