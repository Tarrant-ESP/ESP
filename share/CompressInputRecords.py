#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import os
import sys
import glob
import configparser
import pipes

def floatFromDatetime(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    flSeconds = (dt - epoch).total_seconds()
	
    # total_seconds will be in decimals (millisecond precision)
    return flSeconds

def CompressDirectory(sDir, sSuffix, sMatch, nMonth, nYear):

    sMonth = str(nMonth)
    if nMonth < 10:
        sMonth = "0" + str(nMonth)
    sYear = str(nYear)
   
    sDateMin = sYear + "-" + sMonth + "-01"
    dtMin = datetime.datetime.strptime(sDateMin, "%Y-%m-%d")
    flDateMin = floatFromDatetime(dtMin)
    dtMax = dtMin + datetime.timedelta(days=32)
    dtMax = dtMax.replace(day=1)
    flDateMax = floatFromDatetime(dtMax)

    os.chdir(sDir)
    
    # Build the list of .txt files for the designated month
    sFileList = ""
    try:
        for sFile in glob.glob(sMatch):
            fs = os.stat(sFile)
            ### print(sFile + " " + str(flDateMin) + " " + str(fs.st_ctime) + " " + str(flDateMax))
            if fs.st_ctime >= flDateMin and fs.st_ctime < flDateMax:
                sFileList += " " + pipes.quote(sFile)
                ### print(sFile + " passed ")

        # if the file list is empty
        sFileList = sFileList.strip()
        if(len(sFileList) == 0):
            print("No files to zip in dir: " + sDir)
        else:
            # compress the old files     
            sCmd = ("zip " + sYear + "-" + sMonth + "-" + sSuffix + ".zip " + sFileList)
    
            ### print("command: " + sCmd)
            nRet = os.system(sCmd)
            ### print "return: " + str(nRet)
            # if we created a zip file or there is nothing to do
            if(nRet == 0 or nRet == 3072):
                sCmd = "rm " + sFileList
                ### print("command: " + sCmd)
                nRet = os.system(sCmd)
                if(nRet != 0):
                    sys.stderr.write("Error deleting zipped files in dir: " + sDir)
                    sys.exit(2)
            else:
                sys.stderr.write("Error zipping files in dir: " + sDir)
                sys.exit(3)
            
    except Exception as e:
        sys.stderr.write("dir: " + sDir + " Exception: " + str(e))
        sys.exit(4)

# Get list of directories and search patterns
config = configparser.ConfigParser()
config.read('compress.properties')
sDirList = config["compress"]["dir_list"]
sSuffixList = config["compress"]["suffix_list"]
sPatternList = config["compress"]["pattern_list"]

# these lists are comma delimited, get the elements and make sure the count matches
asDir = sDirList.split(",")
asSuffix = sSuffixList.split(",")
asPattern = sPatternList.split(",")

cDir = len(asDir)
if(cDir != len(asPattern) or cDir != len(asSuffix)):
    sys.stderr.write("Error: Length of directory list, search pattern list and suffix list do not match")
    sys.exit(1)

# Get current month and year, 
# this is designed to be called on the first day of the month
# so we are looking to compress all the files from 2 months ago, leaving last month alone
dtNow = datetime.datetime.now()
nYearCompress = dtNow.year
nMonth = dtNow.month
nMonthCompress = nMonth - 2
###print "month: " + str(nMonthCompress)
###print "year: " + str(nYearCompress)

# Handle year boundaries
if nMonthCompress <= 0:
    nMonthCompress += 12
    nYearCompress -= 1

for iDir in range(cDir):
    CompressDirectory(asDir[iDir], asSuffix[iDir], asPattern[iDir], nMonthCompress, nYearCompress)

sys.exit(0)
    
