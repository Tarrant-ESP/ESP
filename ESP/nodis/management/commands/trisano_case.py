'''
                                  ESP Health
                         Notifiable Diseases Framework
                                Trisano Reporter

@author: Bob Zambarano <bzambarano@commoninf.com>
@organization: Commonwealth informatics http://www.commoninf.com
@copyright: (c) 2019 Commonwealth Informatics
@license: LGPL - http://www.gnu.org/licenses/lgpl-3.0.txt

--------------------------------------------------------------------------------
'''

import datetime
import ftplib
import os
import pprint
import shlex
import socket
import subprocess
import sys, pdb
import time

from datetime import timedelta

from django.core.management.base import BaseCommand
from django.db.models import Q
from django.template import TemplateDoesNotExist
from django.template.loader import render_to_string, get_template
from django.contrib.contenttypes.models import ContentType

from ESP.conf.models import ReportableLab, LabTestMap, ConditionConfig, ReportableMedication, ResultString
from ESP.emr.models import Patient_Addr, Patient_Guardian, LabOrder, LabResult, Encounter, Providernote
from ESP.hef.base import BaseLabResultHeuristic
from ESP.nodis.base import DiseaseDefinition
from ESP.static.models import Loinc, DrugSynonym, Dx_code
from ESP.nodis.models import Case, ReportRun, Report, STATUS_CHOICES, CaseReportReported, CaseReport, Reported
from ESP.settings import CASE_REPORT_OUTPUT_FOLDER, CASE_REPORT_TEMPLATE, CASE_REPORT_BATCH_SIZE
from ESP.settings import CASE_REPORT_TRANSMIT, CASE_REPORT_TRANSPORT_SCRIPT, CASE_REPORT_FILENAME_FORMAT
from ESP.settings import FAKE_PATIENT_MRN, FAKE_PATIENT_SURNAME, CODEDIR, LOG_FILE, UPLOAD_SERVER
from ESP.settings import UPLOAD_USER, UPLOAD_PASSWORD, UPLOAD_PATH
from ESP.utils.utils import log, log_query

#===============================================================================
#
#--- Configuration
#
#===============================================================================

VERSION = '2.3.1'

RACEDIR = {'CAUCASIAN': 'W', 'W': 'W', 'WHITE': 'W', 'BLACK': 'B', 'B': 'B', 
           'BLACK OR AFRICAN AMERICAN': 'B', 'OTHER': 'O',
           'MULTIRACIAL': 'O', 'O': 'O', 'INDIAN': 'I', 'AMERICAN INDIAN/ALASKAN NATIVE': 'I', 'I': 'I',
           'ASIAN': 'A', 'A': 'A', 'NAT AMERICAN': 'I', 'NATIVE HAWAI': 'P',
           'PACIFIC ISLANDER/HAWAIIAN': 'P', 'P': 'P', 'ALASKAN': 'I'
           }

#===============================================================================
#
#--- Exceptions
#
#===============================================================================

class IncompleteCaseData(BaseException):
    '''
    Exception raised when a case does not have all the data elements required 
    to generate a valid HL7 message.
    '''
    pass

#===============================================================================
#
#--- Functions
#
#===============================================================================

def isoTime(t=None, tz=False):
    """ if tz is False, yyyymmddhhmmss - as of now unless a localtime is passed in
        if tz is True, yyyymmddhhmmss [timezone]
        Timezone is local timezone
    """
    if t is None:
        t=time.localtime()
    timeString  = time.strftime("%Y%m%d%H%M%S", t)
    if tz:
        timezone = -(time.altzone if t.tm_isdst else time.timezone)
        timeString += "Z" if timezone == 0 else "+" if timezone > 0 else "-"
        timeString += time.strftime("%H%M", time.gmtime(abs(timezone)))
        return timeString
    else:
        return timeString


class HL7DataObj:
    '''
    Generic data-holding object, with attributes assigned only to a specific instance.
    see https://docs.python.org/2/tutorial/classes.html#odds-and-ends
    ''' 
    pass


class MakeTemplateData:   
    '''
    class for building data required for template processing of HL7 message
    ''' 

    def __init__(self):
        self.timestamp = isoTime()

    def MakePid(self, patient):
        '''
        PID comes from emr_patient table and associated EMR data
        patient ID come from case
        '''
        pid = HL7DataObj()
        pid.pid_3_1 = patient.mrn 
        pid.pid_4_1 = patient.id
        pid.pid_5_1 = patient.last_name
        pid.pid_5_2 = patient.first_name
        pid.pid_5_3 = patient.middle_name
        pid.pid_7_1 = patient.date_of_birth.strftime("%Y%m%d")
        pid.pid_8_1 = patient.gender[0].upper()
        pid.pid_10_1  = race = RACEDIR[patient.race.upper()]
        #Need to get coding for race values (maybe gender too)
        pid.pid_11_1 = patient.address1
        pid.pid_11_2 = patient.address2
        pid.pid_11_3 = patient.city
        pid.pid_11_4 = patient.state
        pid.pid_11_5 = patient.zip
        pid.pid_11_6 = patient.country
        pid.pid_13_6 = patient.areacode
        pid.pid_13_7 = patient.tel
        pid.pid_13_8 = patient.tel_ext
        return pid

    def MakeNk1(self,providers):
        '''
        NK1 is next of kin, but in hl7 2.3.1 it s used for provider information as well
        This is an optional record, but is typically available
        '''
        nk1_lst = []
        for prov in providers:
            nk1 = HL7DataObj()
            nk1.nk1_2_1=prov.last_name
            nk1.nk1_2_2=prov.first_name
            nk1.nk1_2_3=prov.middle_name
            nk1.nk1_4_1=prov.dept_address_1
            nk1.nk1_4_2=prov.dept_address_2
            nk1.nk1_4_3=prov.dept_city
            nk1.nk1_4_4=prov.dept_state
            nk1.nk1_4_5=prov.dept_zip
            nk1.nk1_5_6=prov.area_code
            nk1.nk1_5_7=prov.telephone
            nk1_lst.append(nk1)
        return nk1_lst
 
    def MakeNte(self,reportable,case):
        '''
        This is an optional record
        reportable includes: providernotes, labs, encounters, dx_codes, presecriptions
        '''
        ntes = []
        incr = 0
        for dx in reportable.dx_codes: 
            incr+=1
            nte = HL7DataObj()
            nte.nte_1 = incr
            nte.nte_2 = 'ESP_ICD10_DIAGNOSIS'
            nte.nte_3 = dx.combotypecode
            ntes.append(nte)
        med_names = set(ReportableMedication.objects.filter(condition=case.condition).values_list('drug_name', flat=True))
        for med in reportable.prescriptions:
            report_med = self.reportable_medname(med,med_names)
            incr+=1
            nte = HL7DataObj()
            nte.nte_1 = incr
            nte.nte_2 = 'ESP_TREATMENT'
            nte.nte_3 = report_med
            nte.nte_4 = med.date.strftime("%Y-%m-%d")
            if med.end_date:
                nte.nte_5 = med.end_date.strftime("%Y-%m-%d")
            ntes.append(nte)            
        for note in reportable.providernotes:
            incr+=1
            nte = HL7DataObj()
            nte.nte_1 = incr
            nte.nte_2 = 'ESP_PHYSICIAN_NOTE'
            nte.nte_3 = note.provider_note
            nte.nte_4 = note.date.strftime("%Y-%m-%d")
            ntes.append(nte)            
        recent_encounters = Encounter.objects.filter(patient=case.patient,date__gte=datetime.datetime.now()-datetime.timedelta(days=14))
        for enc in recent_encounters:
            if enc.pregnant==TRUE:
                status='POSITIVE'
                incr+=1
                nte = HL7DataObj()
                nte.nte_1 = incr
                nte.nte_2 = 'ESP_PREGNANCY_STATUS'
                nte.nte_3 = 'POSTIVE'
                nte.nte_4 = enc.date.strftime("%Y-%m-%d")
                ntes.append(nte)
                break
        return ntes
    
    def MakeObxL(self,labs,case):
        '''
        OBX is the observation record, you can have them for Lab results, or specimen observations.
        This version is for labs
        '''
        incr=1
        obxs = []
        for lab in labs:
            if not lab.result_float and not lab.result_string:
                continue
            obx = HL7DataObj()
            incr+=1
            obx.obx_1 = incr
            if lab.result_float is None:
                obx.obx_2 = 'CE'
            else:
                obx.obx_2 = 'SN'
            snomed, snomed2, titer_dilution, finding = self.getSNOMED(lab, case.condition)
            obx.obx_3_1 = lab.output_or_native_code
            obx.obx_3_2 = Loinc.objects.values_list('shortname',flat=True).get(loinc_num=lab.output_or_native_code)
            obx.obx_3_3 = 'LN'
            obx.obx_5_1 = snomed
            obx.obx_5_2 = finding
            obx.obx_5_3 = 'SNM'
            obx.obx_7 = 'NEGATIVE'
            obx.obx_11 = lab.status[0]
            obx.obx_14 = lab.date.strftime("%Y%m%d")
            obxs.append(obx)
        return obxs
    
    def getSNOMED(self, lxRec, condition):
        # returns four values related to lab finding used in MakeOBXL
        # the return statements 
        snomed = None
        snomed2 = None
        titer_dilution = None
        finding = None
        snomedposi = lxRec.snomed_pos
        snomednega = lxRec.snomed_neg
        snomedinter = lxRec.snomed_ind
        if snomedposi in [None, ''] and snomednega in [None, '']:  ##like ALT/AST
            return snomed, snomed2, titer_dilution, finding
        # we have to get the titer dilution level for positive results from
        # the lab heuristic in order to determine if a titer lab is positive.
        # This is clunky.  BaseLabResultsHeuristic.get_all returns a set of heuristic
        # objects, some of which have the titer_dilution attribute, and some of 
        # those have value titer_dilution values.
        titerHset = set(
            h for h in BaseLabResultHeuristic.get_all() if hasattr(h, 'titer_dilution') and h.titer_dilution)
        for h in titerHset:
            try:
                if LabTestMap.objects.filter(test_name=h.test_name, native_code=lxRec.native_code).exists():
                    titer_dilution = h.titer_dilution
                    continue
            except:
                msg = 'heuristic %s is mapped to test_name %s but no such test mapped in Labtestmap' % (
                str(h), h.test_name)
                log.debug(msg)
                # this breaks if a lab test can be part of more than one test heuristic, AND titer dilution level is different over these heuristics.
        if titer_dilution:
            pos_titer_list = ['1:%s' % 2 ** i for i in
                              range(int(math.log(4096, 2)), int(math.log(titer_dilution, 2)) - 1, -1)
                              ]
            pos_match = next((s for s in pos_titer_list if s in lxRec.result_string), None)
            if pos_match is not None:
                snomed = snomedposi
                finding = 'POSITIVE'
                snomed2 = TITER_DILUTION_CHOICES[pos_match]
                return snomed, snomed2, titer_dilution, finding
            neg_titer_list = ['1:%s' % 2 ** i for i in range(int(math.log(titer_dilution, 2)), -1, -1)]
            neg_match = next((s for s in neg_titer_list if s in lxRec.result_string), None)
            if neg_match is not None:
                snomed = snomednega
                finding = 'neg'
                snomed2 = TITER_DILUTION_CHOICES[neg_match]
                return snomed, snomed2, titer_dilution, finding
        # now check the case lx events to see if this lab matches one of those -- if so, get the finding and set snomed
        try:
            case_lx = Case.objects.get(id=self.currentCase, events__name__startswith='lx', events__object_id=lxRec.id)
            if "positive" in str(case_lx.events.get(object_id=lxRec.id).name):
                snomed = snomedposi
                finding = 'POSITIVE' 
            #very few case heuristics include negative or indeterminate lab results as case events 
            elif "negative" in str(case_lx.events.get(object_id=lxRec.id).name):
                snomed = snomednega
                finding = 'NEGATIVE'
            elif "indeterminate" in str(case_lx.events.get(object_id=lxRec.id).name):
                snomed = snomedinter
                finding = 'INDETERMINATE'
            return snomed, snomed2, titer_dilution, finding
        except:
            # now we essentially replicate hef.base.labresultpositiveheuristic
            if lxRec.result_float is not None and not titer_dilution:
                if lxRec.ref_high_float is not None:
                    if lxRec.result_float < lxRec.ref_high_float:
                        snomed = snomednega
                        finding = 'NEGATIVE'
                    else:
                        snomed = snomedposi
                        finding = 'POSITIVE'
                elif lxRec.codemap.threshold is not None:
                    if lxRec.result_float < lxRec.codemap.threshold:
                        snomed = snomednega
                        finding = 'NEGATIVE'
                    else:
                        snomed = snomedposi
                        finding = 'POSITIVE'
                return snomed, snomed2, titer_dilution, finding
            else:
                # now it gets messy.  The resultstring stuff is designed to build django queryset definitions
                # so we're stuck re-querying for the lab result.
                map_obj = LabTestMap.objects.get(native_code=lxRec.native_code)
                pos_q = ResultString.get_q_by_indication('pos')
                neg_q = ResultString.get_q_by_indication('neg')
                ind_q = ResultString.get_q_by_indication('ind')
                if map_obj.extra_positive_strings.all():
                    pos_q |= map_obj.positive_string_q_obj
                if map_obj.excluded_positive_strings.all():
                    pos_q &= ~map_obj.positive_exclude_string_q_obj
                if map_obj.extra_negative_strings.all():
                    neg_q |= map_obj.negative_string_q_obj
                if map_obj.excluded_negative_strings.all():
                    neg_q &= ~map_obj.negative_exclude_string_q_obj
                if map_obj.extra_indeterminate_strings.all():
                    ind_q |= map_obj.indeterminate_string_q_obj
                if map_obj.excluded_indeterminate_strings.all():
                    ind_q &= ~map_obj.indeterminate_exclude_string_q_obj

                if LabResult.objects.filter(Q(id=lxRec.id), pos_q).exists():
                    snomed = snomedposi
                    finding = 'POSITIVE'
                elif LabResult.objects.filter(Q(id=lxRec.id), neg_q).exists():
                    snomed = snomednega
                    finding = 'NEGATIVE'
                elif LabResult.objects.filter(Q(id=lxRec.id), ind_q).exists():
                    snomed = snomedinter
                    finding = 'INDETERMINATE'
                return snomed, snomed2, titer_dilution, finding

    def reportable_labs(self,case):
        '''
        The version of this in nodis models is specific to MA reporting.  
        '''
        conf = ConditionConfig.objects.get(name=case.condition) # should be only one ConditionConfig per condition
        q_obj = None
        reportable_labnames = set(
            ReportableLab.objects.filter(condition=case.condition).values_list('native_name', flat=True))
        if reportable_labnames:
            reportable_codes = set()
            for labname in reportable_labnames:
                reportable_codes |= set(
                    LabTestMap.objects.filter(test_name=labname, reportable=True).values_list('native_code',
                                                                                              flat=True))
            q_obj = Q(patient=self.patient)
            q_obj &= Q(native_code__in=reportable_codes)
            start = self.date - datetime.timedelta(days=conf.lab_days_before)
            end = self.date + datetime.timedelta(days=conf.lab_days_after)
            q_obj &= Q(date__gte=start)
            q_obj &= Q(date__lte=end)
            labs = LabResult.objects.filter(q_obj).distinct()
            labs |= case.heuristics_labs
        else:
            labs = case.heuristics_labs

        return labs.distinct()

    def reportable_notes(self,case):
        '''
        Unique to Tarrant physician_notes object.  
        '''
        notes = Providernote.objects.filter(patient=case.patient_id,date__gte=case.date-timedelta(days=14),date__lte=case.date+timedelta(days=14))
        return notes

    def reportable_medname(self,prescription, reportables):
        '''
        takes a prescription and either returns the reportable name, or if not configured, the generic name
        '''
        other_names = DrugSynonym.objects.all()
        #really there should only be one generic_name here, but just in case...
        generics = set()
        #find the set of generic names for drugs that match our prescription
        for other_name in other_names:
            if other_name.other_name.lower() in prescription.name.lower():
                generics.add(other_name.generic_name)
        generic_set = DrugSynonym.objects.filter(generic_name__in=generics)
        #now see if any of the other values match a reportable, if so return it.
        for genericRec in generic_set:
            if genericRec.other_name in reportables:
                return genericRec.other_name
        return generics[0]

    def dx_from_encounters(self, encounters, dxs):
        dx = Dx_code.objects.none()
        for encounter in encounters:
            dx |= encounter.dx_codes.filter(combotypecode__in=dxs.values_list('combotypecode',flat=True))
        return dx


    def get_unreported(self,case):
        '''
        the version of this in nodis models uses the MA specific reportable labs
        '''
        from collections import namedtuple
        reported = Reported.from_case(case.pk)
        Unreported = namedtuple('Unreported','encounters, dx_codes, prescriptions, labs, providernotes')
        encounters, dxs = case.reportable_encounters
        unreported_encounters = case.unreported(encounters, reported)
        dx_codes = self.dx_from_encounters(unreported_encounters, dxs)
        reportable_notes = self.reportable_notes(case)

        unreported = Unreported(encounters=unreported_encounters,
                                dx_codes=dx_codes,
                                prescriptions=case.unreported(case.reportable_prescriptions, reported),
                                labs=case.unreported(self.reportable_labs(case), reported),
                                providernotes=case.unreported(reportable_notes, reported))
        return unreported


    def MakeHL7Data(self,rawcase):
        '''
        This fills the data object that is used by the template
        '''
        currentCase = rawcase.id
        report = CaseReportReported()
        report.case_report, created = CaseReport.objects.get_or_create(case_id = currentCase)
        report.save()
        case_report = report.case_report
        case = HL7DataObj()
        case.id = rawcase.id
        case.date = rawcase.date
        case.status = rawcase.status
        case.pid = self.MakePid(rawcase.patient) 
        unreported = self.get_unreported(rawcase) #providernotes, labs, encounters, dx_codes, presecriptions
        reported = Reported.from_case(rawcase.pk)
        reported_providers=set()
        for item in reported:
            ctype=ContentType.objects.get(pk=item.content_type_id)
            reported_providers.add(ctype.get_object_for_this_type(id=item.object_id).provider)
        unreported_providers=set()
        for lab in unreported.labs:
            unreported_providers.add(lab.provider)
        for enc in unreported.encounters:
            unreported_providers.add(enc.provider)
        for med in unreported.prescriptions:
            unreported_providers.add(med.provider)
        providers=unreported_providers - reported_providers
        case.nk1 = self.MakeNk1(providers)
        case.ntes = self.MakeNte(unreported, rawcase)
        case.obxs = self.MakeObxL(unreported.labs, rawcase)

        for reported in unreported.encounters:
            Reported.set_reported(report, reported)
        for reported in unreported.labs:
            Reported.set_reported(report, reported)
        for reported in unreported.providernotes:
            Reported.set_reported(report, reported)
        for reported in unreported.prescriptions:
            Reported.set_reported(report, reported)

        return case

class Command(BaseCommand):
    
    help = 'Generate case message for TriSano EMSA'
    
    def add_arguments(self, parser):
        parser.add_argument('conditions', nargs='*', type=str) #positional argument - list of conditions at end of command line string
        parser.add_argument('--case', action='store', dest='case_id', type=int, metavar='ID',
            help='Export a single case with specified case ID')
        parser.add_argument('--status', action='store', dest='status', default='Q',
            help='Export only cases with this status ("Q" by default)')
        parser.add_argument('--batch-size', action='store', type=int, dest='batch_size', metavar='NUM',
            default=1, help='Generate batches of NUM cases per file')
        parser.add_argument('--transmit', action='store_true', dest='transmit', default=False,
            help='Transmit cases after generation')
        parser.add_argument('--no-mark-sent', action='store_false', dest='mark_sent', default=True,
            help='Do NOT set cases status to "S"')
        parser.add_argument('-o', action='store', metavar='FOLDER', dest='output_folder',
            default=CASE_REPORT_OUTPUT_FOLDER, help='Output case report file(s) to FOLDER')
        parser.add_argument('-f', action='store', dest='format', metavar='FORMAT', default=CASE_REPORT_FILENAME_FORMAT,
            help='Create file names using FORMAT.  Default: %s' % CASE_REPORT_FILENAME_FORMAT)
        parser.add_argument('--individual', action='store_false', dest='one_file',
            default=False, help='Export each case to an individual file (default)')

    
    def handle(self, *args, **options):
        output_file_paths = [] # Full path to each output file
        report_conditions = [] # Names of conditions for which we will export cases
        #
        # Parse and sanity check command line for options
        #
        all_conditions = DiseaseDefinition.get_all_conditions()
        all_conditions.sort()
        if not options['conditions']:
            report_conditions = all_conditions
        else:
            for a in options['conditions']:
                print a
                if a in all_conditions:
                    report_conditions.append(a)
                else:
                    print >> sys.stderr
                    print >> sys.stderr, 'Unrecognized condition: "%s".  Aborting.' % a
                    print >> sys.stderr
                    print >> sys.stderr, 'Valid conditions are:'
                    print >> sys.stderr, '    --------'
                    print >> sys.stderr, '    all (reports all conditions below)'
                    print >> sys.stderr, '    --------'
                    for con in all_conditions:
                        print >> sys.stderr, '    %s' % con
                    sys.exit(101)

        log.debug('conditions: %s' % report_conditions)
        valid_status_choices = [item[0] for item in STATUS_CHOICES]
        if options['status'] not in valid_status_choices:
                print >> sys.stderr
                print >> sys.stderr, 'Unrecognized status: "%s".  Aborting.' % options['status']
                print >> sys.stderr
                print >> sys.stderr, 'Valid status choices are:'
                for stat in valid_status_choices:
                    print >> sys.stderr, '    %s' % stat
                sys.exit(102)
        log.debug('status: %s' % options['status'])
        #
        # Set up case report run object
        #
        run = ReportRun(hostname=socket.gethostname())
        run.save()
        #
        # Generate case query
        #
        if options['case_id']:
            q_obj = Q(pk__exact=options['case_id'])
        else:
            q_obj = Q(condition__in=report_conditions)
            q_obj = q_obj & Q(status=options['status'])
        cases = Case.objects.filter(q_obj).order_by('pk')
        log_query('Filtered cases', cases)
        
        if not cases:
            msg = 'No cases found matching your specifications.  No output generated.'
            log.info(msg)
            print >> sys.stderr, ''
            print >> sys.stderr, msg
            print >> sys.stderr, ''
            sys.exit()
        else:
            for case in cases:
                #
                # Generate report message
                #
                report_str = self.use_template(options, case)
                log.debug('Message to report:\n%s' % report_str)
                report_obj = Report(
                    run = run,
                    message = report_str,)
                formatdict = {'timestamp': datetime.datetime.now().strftime('%Y-%b-%d.%H.%M.%s')}
                filename = options['format'] % formatdict
                report_obj.filename = filename
                filepath = os.path.join(options['output_folder'], filename)
                output_file_paths.append(filepath)
                file = open(filepath, 'w')
                file.write(report_str)
                file.close()
                log.info('Wrote case report to file: %s' % filepath)
            #
                if options['mark_sent']:
                    if (case.status == 'RQ'):
                        case.status = 'RS'
                    else:
                        case.status = 'S'
                    case.sent_timestamp = datetime.datetime.now()
                    case.save()
                #something about reported here...
                log.debug("Set status to 'S' or 'RS' for this batch of cases")
                report_obj.sent = True
                report_obj.save()
                caseqs=Case.objects.filter(id=case.id) ##report_obj.cases needs iterable.
                report_obj.cases = caseqs # 'Report' instance needs to have a primary key value before a many-to-many relationship can be used.
                report_obj.save()
        #
        # Print the full path of each output file, for the consumption of any script that may call this command.
        #
        for path in output_file_paths:
            print path
    
    def use_template(self, options, case):
        '''
        Generate report messages based on a template
        '''
        template_name = os.path.join('TriSano_interface', CASE_REPORT_TEMPLATE)
        log.debug('template_name: %s' % template_name)
        try:
            get_template(template_name)
        except TemplateDoesNotExist:
            print >> sys.stderr
            print >> sys.stderr, 'Unrecognized template name: "%s".  Aborting.' % CASE_REPORT_TEMPLATE
            print >> sys.stderr
            sys.exit(103)
        #
        # Build report message 
        #
        hl7 = MakeTemplateData()
        values = {
            'case': hl7.MakeHL7Data(case),
            }
        log.debug('values for template: \n%s' % pprint.pformat(values))
        case_report = render_to_string(template_name, values)
        # Remove blank lines -- allows us to have neater templates
        case_report = '\n'.join([x for x in case_report.split("\n") if x.strip()!=''])
        return case_report

