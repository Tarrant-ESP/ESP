from datetime import date, timedelta

from django.test import TestCase

from ESP.conf.models import ConditionConfig, ReportableDx_Code, ReportableLab, LabTestMap
from ESP.emr.models import Patient, Provider, LabResult, Encounter
from ESP.hef.models import Event
from ESP.nodis.management.commands.case_report import hl7Batch, Command
from ESP.nodis.models import Case
from ESP.static.models import Dx_code


class Na5DXCodeTestCase(TestCase):
    condition_name = 'test_cond'

    @classmethod
    def setUpTestData(cls):
        cls.patient = Patient.objects.create(provenance_id=1)
        cls.provider = Provider.objects.create(provenance_id=1)
        cls.condition = ConditionConfig.objects.create(name=cls.condition_name)
        cls.dx_code = Dx_code.objects.create(combotypecode='TEST:TEST', code="TEST", type="TEST", name="test",
                                             longname="obx_test_dx")
        ReportableDx_Code.objects.create(condition=cls.condition, dx_code=cls.dx_code)
        ReportableLab.objects.create(condition=cls.condition, native_name=cls.condition_name)
        LabTestMap.objects.create(test_name=cls.condition_name, reportable=True, native_code=cls.condition_name)

    def setUp(self):
        today = date.today()
        self.case_date = today - timedelta(days=72)
        self.case = Case.objects.create(condition=self.condition_name, patient=self.patient,
                                        provider=self.provider,
                                        date=self.case_date)

        for pid in range(2):
            lab = LabResult.objects.create(date=self.case_date, patient=self.patient, provenance_id=pid,
                                           provider=self.provider, result_date=self.case_date,
                                           native_code=self.condition_name, native_name=self.condition_name,
                                           result_string="Positive")
            event = Event.create('test:lab:event', 'tests', lab.date, self.patient, self.provider, lab)[1][0]
            self.case.events.add(event)

        self.hl7 = hl7Batch()
        self.hl7.currentCase = self.case.id

        self.command = Command()

    def addEncounter(self, offset=0):
        self.encounter = Encounter.objects.create(patient=self.patient, provider=self.provider,
                                                  date=self.case_date - timedelta(days=offset), provenance_id=1)
        self.encounter.dx_codes.add(self.dx_code)

    def test_symptom_obx_previous_yes(self):
        result = self.hl7.symptom_obx(0, na_5=True)
        self.assertEqual(result.get('obx5'), [('CE.4', self.hl7.snomed['YES'])])

    def test_symptom_obx_no_lx_or_encounters(self):
        result = self.hl7.symptom_obx(0, na_5=False)
        self.assertEqual(result.get('obx5'), [('CE.4', self.hl7.snomed['NO'])])

    def test_symptom_obx_only_lx(self):
        result = self.hl7.symptom_obx(0, lx=self.case.reportable_labs.first())
        self.assertEqual(result.get('obx5'), [('CE.4', self.hl7.snomed['NO'])])

    def test_symptom_obx_only_encounter(self):
        self.addEncounter()
        result = self.hl7.symptom_obx(0, encounters=[self.encounter])
        self.assertEqual(result.get('obx5'), [('CE.4', self.hl7.snomed['YES'])])

    def test_symptom_obx_lx_encounter_within_14_days(self):
        self.addEncounter(12)
        result = self.hl7.symptom_obx(0, lx=self.case.reportable_labs.first(), encounters=[self.encounter])
        self.assertEqual(result.get('obx5'), [('CE.4', self.hl7.snomed['YES'])])

    def test_symptom_obx_lx_encounter_outside_14_days(self):
        self.addEncounter(15)
        result = self.hl7.symptom_obx(0, lx=self.case.reportable_labs.first(), encounters=[self.encounter])
        self.assertEqual(result.get('obx5'), [('CE.4', self.hl7.snomed['NO'])])

    def test_dx_code_obx(self):
        self.addEncounter()
        dxs = self.case.dx_from_encounters(self.case.reportable_encounters[0])
        result = self.hl7.dx_code_obx(0, dx_code=dxs)
        self.assertEqual(len(result), 1)

    def test_dx_inside_symptom_window(self):
        self.addEncounter()
        dxs = self.case.dx_from_encounters(self.case.reportable_encounters[0])
        self.encounter.date = self.case_date - timedelta(days=12)

        symptom = self.hl7.symptom_obx(0, lx=self.case.reportable_labs.first(), encounters=[self.encounter])
        self.assertEqual(symptom.get('obx5'), [('CE.4', self.hl7.snomed['YES'])])

        dx = self.hl7.dx_code_obx(0, dx_code=dxs)
        self.assertEqual(len(dx), 1)

    def test_dx_outside_symptom_window(self):
        self.addEncounter(27)

        symptom = self.hl7.symptom_obx(0, lx=self.case.reportable_labs.first(), encounters=[self.encounter])
        self.assertEqual(symptom.get('obx5'), [('CE.4', self.hl7.snomed['NO'])])

        dxs = self.case.dx_from_encounters(self.case.reportable_encounters[0])
        dx = self.hl7.dx_code_obx(0, dx_code=dxs)
        self.assertEqual(len(dx), 1)

    def test_dx_in_symptom_window_by_mdph(self):
        self.addEncounter()
        report = ''.join(self.command.mdph(False, None, [self.case]).split())
        self.assertTrue(
            "<OBX.3><CE.4>NA-5</CE.4></OBX.3><OBX.5><CE.4>{}</CE.4></OBX.5>".format(self.hl7.snomed['YES']) in report)
        self.assertTrue(
            "10187-3" in report)

    def test_dx_outside_symptom_window_by_mdph(self):
        self.addEncounter(20)
        report = ''.join(self.command.mdph(False, None, [self.case]).split())
        self.assertTrue(
            "<OBX.3><CE.4>NA-5</CE.4></OBX.3><OBX.5><CE.4>{}</CE.4></OBX.5>".format(self.hl7.snomed['NO']) in report)
        self.assertTrue(
            "10187-3" in report)

    def test_dx_outside_reportable_window_by_mdph(self):
        self.addEncounter(30)
        report = ''.join(self.command.mdph(False, None, [self.case]).split())
        self.assertFalse(
            "10187-3" in report)

    def test_dx_inside_reportable_window_by_mdph(self):
        self.addEncounter(28)
        report = ''.join(self.command.mdph(False, None, [self.case]).split())
        self.assertTrue(
            "10187-3" in report)
