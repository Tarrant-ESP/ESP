# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('nodis', '0008_auto_20180305_1123'),
    ]

    operations = [
        migrations.AlterField(
            model_name='case',
            name='status',
            field=models.CharField(default=b'AR', max_length=32, verbose_name=b'Case status', choices=[(b'AR', b'AR - Awaiting Review'), (b'UR', b'UR - Under Review'), (b'RM', b'RM - Review by MD'), (b'FP', b'FP - False Positive - Do NOT Process'), (b'Q', b'Q - Confirmed Case, Queued for Snapshot'), (b'S', b'S - Snapshot Taken'), (b'U', b'U - Updated with new data'), (b'NO', b'NO - Do NOT send cases'), (b'RQ', b'RQ - Re-queued for transmission. Updated after prior transmission'), (b'RS', b'RS - Re-sent after update subsequent to prior transmission')]),
        ),
        migrations.AlterField(
            model_name='casestatushistory',
            name='new_status',
            field=models.CharField(max_length=10, choices=[(b'AR', b'AR - Awaiting Review'), (b'UR', b'UR - Under Review'), (b'RM', b'RM - Review by MD'), (b'FP', b'FP - False Positive - Do NOT Process'), (b'Q', b'Q - Confirmed Case, Queued for Snapshot'), (b'S', b'S - Snapshot Taken'), (b'U', b'U - Updated with new data'), (b'NO', b'NO - Do NOT send cases'), (b'RQ', b'RQ - Re-queued for transmission. Updated after prior transmission'), (b'RS', b'RS - Re-sent after update subsequent to prior transmission')]),
        ),
        migrations.AlterField(
            model_name='casestatushistory',
            name='old_status',
            field=models.CharField(max_length=10, choices=[(b'AR', b'AR - Awaiting Review'), (b'UR', b'UR - Under Review'), (b'RM', b'RM - Review by MD'), (b'FP', b'FP - False Positive - Do NOT Process'), (b'Q', b'Q - Confirmed Case, Queued for Snapshot'), (b'S', b'S - Snapshot Taken'), (b'U', b'U - Updated with new data'), (b'NO', b'NO - Do NOT send cases'), (b'RQ', b'RQ - Re-queued for transmission. Updated after prior transmission'), (b'RS', b'RS - Re-sent after update subsequent to prior transmission')]),
        ),
    ]
