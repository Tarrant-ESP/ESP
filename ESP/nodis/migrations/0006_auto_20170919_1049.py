# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nodis', '0005_auto_20170918_1515'),
    ]

    operations = [
        migrations.AlterField(
            model_name='casereportreported',
            name='time_sent',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
