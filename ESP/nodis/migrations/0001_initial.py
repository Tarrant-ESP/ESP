# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0001_initial'),
        ('hef', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Case',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('condition', models.CharField(max_length=100, verbose_name=b'Common English name for this medical condition', db_index=True)),
                ('date', models.DateField(db_index=True)),
                ('criteria', models.CharField(db_index=True, max_length=2000, null=True, verbose_name=b'Criteria on which case was diagnosed', blank=True)),
                ('source', models.CharField(max_length=255, verbose_name=b'What algorithm created this case?')),
                ('status', models.CharField(default=b'AR', max_length=32, verbose_name=b'Case status', choices=[(b'AR', b'AR - Awaiting Review'), (b'UR', b'UR - Under Review'), (b'RM', b'RM - Review by MD'), (b'FP', b'FP - False Positive - Do NOT Process'), (b'Q', b'Q - Confirmed Case, Transmit to Health Department'), (b'S', b'S - Transmitted to Health Department'), (b'NO', b'NO - Do NOT send cases'), (b'RQ', b'RQ - Re-queued for transmission. Updated after prior transmission'), (b'RS', b'RS - Re-sent after update subsequent to prior transmission')])),
                ('notes', models.TextField(null=True, blank=True)),
                ('reportables', models.TextField(null=True, blank=True)),
                ('created_timestamp', models.DateTimeField(auto_now_add=True)),
                ('updated_timestamp', models.DateTimeField(auto_now=True)),
                ('sent_timestamp', models.DateTimeField(null=True, blank=True)),
                ('followup_sent', models.BooleanField(default=False, verbose_name=b'Followup event sent?')),
                ('events', models.ManyToManyField(related_name=b'case', to='hef.Event')),
                ('followup_events', models.ManyToManyField(related_name=b'followup', to='hef.Event')),
                ('patient', models.ForeignKey(to='emr.Patient')),
                ('provider', models.ForeignKey(to='emr.Provider')),
                ('timespans', models.ManyToManyField(to='hef.Timespan')),
            ],
            options={
                'ordering': ['id'],
                'permissions': [('view_phi', 'Can view protected health information')],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CaseStatusHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(auto_now=True, db_index=True)),
                ('old_status', models.CharField(max_length=10, choices=[(b'AR', b'AR - Awaiting Review'), (b'UR', b'UR - Under Review'), (b'RM', b'RM - Review by MD'), (b'FP', b'FP - False Positive - Do NOT Process'), (b'Q', b'Q - Confirmed Case, Transmit to Health Department'), (b'S', b'S - Transmitted to Health Department'), (b'NO', b'NO - Do NOT send cases'), (b'RQ', b'RQ - Re-queued for transmission. Updated after prior transmission'), (b'RS', b'RS - Re-sent after update subsequent to prior transmission')])),
                ('new_status', models.CharField(max_length=10, choices=[(b'AR', b'AR - Awaiting Review'), (b'UR', b'UR - Under Review'), (b'RM', b'RM - Review by MD'), (b'FP', b'FP - False Positive - Do NOT Process'), (b'Q', b'Q - Confirmed Case, Transmit to Health Department'), (b'S', b'S - Transmitted to Health Department'), (b'NO', b'NO - Do NOT send cases'), (b'RQ', b'RQ - Re-queued for transmission. Updated after prior transmission'), (b'RS', b'RS - Re-sent after update subsequent to prior transmission')])),
                ('changed_by', models.CharField(max_length=30)),
                ('comment', models.TextField(null=True, blank=True)),
                ('case', models.ForeignKey(to='nodis.Case')),
            ],
            options={
                'verbose_name': 'Case Status History',
                'verbose_name_plural': 'Case Status Histories',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReferenceCase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('condition', models.CharField(max_length=100, db_index=True)),
                ('date', models.DateField(db_index=True)),
                ('ignore', models.BooleanField(default=False, db_index=True)),
                ('notes', models.TextField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Reference Case',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReferenceCaseList',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('source', models.CharField(max_length=255)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('notes', models.TextField(null=True, blank=True)),
            ],
            options={
                'verbose_name': 'Reference Case List',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Report',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(auto_now=True)),
                ('filename', models.CharField(max_length=512)),
                ('sent', models.BooleanField(default=False, verbose_name=b'Case status was set to sent?')),
                ('message', models.TextField(verbose_name=b'Case report message')),
                ('cases', models.ManyToManyField(to='nodis.Case')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReportRun',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(auto_now=True)),
                ('hostname', models.CharField(max_length=255, verbose_name=b'Host on which data was loaded')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ValidatorResult',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('condition', models.CharField(max_length=100, db_index=True)),
                ('date', models.DateField(db_index=True)),
                ('disposition', models.CharField(max_length=30, choices=[(b'exact', b'Exact'), (b'similar', b'Similar'), (b'missing', b'Missing'), (b'new', b'New')])),
                ('cases', models.ManyToManyField(to='nodis.Case', null=True, blank=True)),
                ('encounters', models.ManyToManyField(to='emr.Encounter', null=True, blank=True)),
                ('events', models.ManyToManyField(to='hef.Event', null=True, blank=True)),
                ('lab_results', models.ManyToManyField(to='emr.LabResult', null=True, blank=True)),
                ('prescriptions', models.ManyToManyField(to='emr.Prescription', null=True, blank=True)),
                ('ref_case', models.ForeignKey(blank=True, to='nodis.ReferenceCase', null=True)),
            ],
            options={
                'verbose_name': 'Validator Result',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ValidatorRun',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('complete', models.BooleanField(default=False)),
                ('related_margin', models.IntegerField()),
                ('notes', models.TextField(null=True, blank=True)),
                ('list', models.ForeignKey(to='nodis.ReferenceCaseList')),
            ],
            options={
                'verbose_name': 'Validator Run',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='validatorresult',
            name='run',
            field=models.ForeignKey(to='nodis.ValidatorRun'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='report',
            name='run',
            field=models.ForeignKey(to='nodis.ReportRun'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='referencecase',
            name='list',
            field=models.ForeignKey(to='nodis.ReferenceCaseList'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='referencecase',
            name='patient',
            field=models.ForeignKey(blank=True, to='emr.Patient', null=True),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='case',
            unique_together=set([('patient', 'condition', 'date', 'source')]),
        ),
    ]
