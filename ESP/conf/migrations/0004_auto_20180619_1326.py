# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.exceptions import ObjectDoesNotExist
from django.db import migrations


def add_reportable_dx(apps, schema_editor):
    ReportableDxCode = apps.get_model("conf", "ReportableDx_Code")
    Condition = apps.get_model("conf", "ConditionConfig")
    DX = apps.get_model("static", "Dx_code")

    data = {'gonorrhea': ['icd9:780.60', 'icd9:788.7', 'icd9:099.40', 'icd9:597.80', 'icd9:780.6', 'icd9:616.0',
                          'icd9:616.10', 'icd9:623.5', 'icd9:789.07', 'icd9:789.04', 'icd9:789.09', 'icd9:789.03',
                          'icd9:789.00', 'icd10:R50.9', 'icd10:R36.0', 'icd10:R36.9', 'icd10:N34.1', 'icd10:N34.2',
                          'icd10:N76.0', 'icd10:N76.1', 'icd10:N76.2', 'icd10:N76.3', 'icd10:N72', 'icd10:N89.8',
                          'icd10:R10.84', 'icd10:R10.32', 'icd10:R10.10', 'icd10:R10.2', 'icd10:R10.30', 'icd10:R10.31',
                          'icd10:R10.9', 'icd9:099.54', 'icd10:A56.19', 'icd9:V01.6', 'icd10:Z20.2', 'icd9:V74.5',
                          'icd10:Z11.3'],
            'chlamydia': ['icd9:788.7', 'icd9:099.40', 'icd9:780.6', 'icd9:597.80', 'icd9:616.0', 'icd9:616.10',
                          'icd9:623.5', 'icd9:789.07', 'icd9:789.04', 'icd9:789.09', 'icd9:789.03', 'icd9:789.00',
                          'icd9:780.60', 'icd10:R50.9', 'icd10:R36.0', 'icd10:R36.9', 'icd10:N34.1', 'icd10:N34.2',
                          'icd10:N76.0', 'icd10:N76.1', 'icd10:N76.2', 'icd10:N76.3', 'icd10:N72', 'icd9:099.54',
                          'icd9:V01.6', 'icd9:V74.5', 'icd10:N89.8', 'icd10:R10.84', 'icd10:R10.32', 'icd10:R10.10',
                          'icd10:R10.2', 'icd10:R10.30', 'icd10:R10.31', 'icd10:R10.9', 'icd10:A56.19', 'icd10:A74.9',
                          'icd10:Z20.2', 'icd10:Z11.3']}

    for k in data.keys():
        try:
            cond = Condition.objects.get(name=k)
            for v in data.get(k):
                try:
                    dx = DX.objects.get(combotypecode=v)
                    ReportableDxCode.objects.get_or_create(condition=cond, dx_code=dx)
                except ObjectDoesNotExist:
                    continue
        except ObjectDoesNotExist:
            continue


class Migration(migrations.Migration):
    dependencies = [
        ('conf', '0003_auto_20160513_1020'),
    ]

    operations = [
        migrations.RunPython(add_reportable_dx),
    ]
