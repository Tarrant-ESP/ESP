# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('conf', '0004_auto_20180619_1326'),
    ]

    operations = [
        migrations.CreateModel(
            name='Menu',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('menuname', models.CharField(max_length=100)),
                ('url', models.CharField(max_length=300)),
                ('menu_text', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('code', models.CharField(max_length=3, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Statustransitions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('endcode', models.ForeignKey(related_name='endcode', to='conf.Status')),
                ('startcode', models.ForeignKey(related_name='startcode', to='conf.Status')),
            ],
        ),
        migrations.AlterModelOptions(
            name='hl7map',
            options={'verbose_name_plural': 'HL7 Maps'},
        ),
        migrations.AlterModelOptions(
            name='reportableextended_variables',
            options={'verbose_name': 'Reportable Extended Variables', 'verbose_name_plural': 'Reportable Extended Variables'},
        ),
        migrations.AddField(
            model_name='extended_variablesmap',
            name='value_type',
            field=models.BooleanField(default=False, verbose_name=b'Variable sends value and not mapped code?'),
        ),
        migrations.AlterField(
            model_name='conditionconfig',
            name='initial_status',
            field=models.CharField(default=b'AR', max_length=8, choices=[(b'AR', b'AR - Awaiting Review'), (b'UR', b'UR - Under Review'), (b'RM', b'RM - Review by MD'), (b'FP', b'FP - False Positive - Do NOT Process'), (b'Q', b'Q - Confirmed Case, Queued for Snapshot'), (b'S', b'S - Snapshot Taken'), (b'U', b'U - Updated with new data'), (b'NO', b'NO - Do NOT send cases'), (b'RQ', b'RQ - Re-queued for transmission. Updated after prior transmission'), (b'RS', b'RS - Re-sent after update subsequent to prior transmission')]),
        ),
    ]
