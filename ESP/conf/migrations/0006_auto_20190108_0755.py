# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.core.management import call_command

def load_fixture(apps, schema_editor):
    call_command('loaddata', 'menu_status_transitions', app_label='conf') 

class Migration(migrations.Migration):

    dependencies = [
        ('conf', '0005_tarrant_conf'),
    ]

    operations = [
        migrations.RunPython(load_fixture),
    ]
