'''
                              ESP Health Project
                             Configuration Module
                               URL Configuration

@authors: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory http://www.channing.harvard.edu
@copyright: (c) 2009 Channing Laboratory
@license: LGPL
'''


from django.conf.urls import url
from ESP.conf import views

urlpatterns = [
    url(r'^codes/ignore/(?P<native_code>.+)/$', views.ignore_code, name='ignore_code'),
    #change name worst case .. good point TODO 
    url(r'^codes/reportables/', views.heuristic_reportables, name='heuristic_reportables'),
    url(r'^codes/report', views.heuristic_mapping_report, name='heuristic_mapping_report'),
    #url(r'^json_code_grid', json_code_grid, name='json_code_grid'),
]
