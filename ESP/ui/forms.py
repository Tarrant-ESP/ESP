'''
                              ESP Health Project
                         Notifiable Diseases Framework
                              User Interface Forms


@authors: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory http://www.channing.harvard.edu
@copyright: (c) 2009 Channing Laboratory
@license: LGPL
'''

from django import forms
from django.db.models import BLANK_CHOICE_DASH

from ESP.hef.base import AbstractLabTest
from ESP.nodis.base import DiseaseDefinition
from ESP.nodis.models import STATUS_CHOICES
from ESP.vaers.heuristics import VaersLxHeuristic
from ESP.conf.models import Status


class GenericStatusForm(forms.Form):
    '''

    Form for pertussis details (initially)

    '''

    def __init__(self, curstat, *args, **kwargs):
        super(GenericStatusForm, self).__init__(*args, **kwargs)
        self.fields['comment'] = forms.CharField(widget=forms.Textarea, required=False)
        try:
            status_choice_codes = Status.objects.get(code__exact=curstat).startcode.all().values_list('endcode',
                                                                                                      flat=True)
        except:
            status_choice_codes = STATUS_CHOICES
        self.fields['status'] = forms.ModelChoiceField(queryset=Status.objects.filter(code__in=status_choice_codes),
                                                       required=False, )


class CaseStatusForm(forms.Form):
    '''
    Form for mapping Native Code to LOINC
    '''
    status = forms.ChoiceField(STATUS_CHOICES[:4], required=False)
    comment = forms.CharField(widget=forms.Textarea, required=False)


class CodeMapForm(forms.Form):
    # fake instanciation to get the labs in the drop down
    vaerslabs = VaersLxHeuristic('wbc', None, None, None)
    TEST_CHOICES = [(name, name) for name in AbstractLabTest.get_all_names()]
    # FIXING MULTIPLE BILIRUBINS IN LIST 
    for name in vaerslabs.get_all_names():
        if (name, name) not in TEST_CHOICES:
            TEST_CHOICES.append((name, name))

    TEST_CHOICES.sort()
    test_name = forms.ChoiceField(choices=BLANK_CHOICE_DASH + TEST_CHOICES, required=True)
    threshold = forms.FloatField(required=False)
    notes = forms.CharField(widget=forms.Textarea, required=False)
    output_code = forms.CharField(label='LOINC', max_length=100, required=False)
    snomed_pos = forms.CharField(label='SNOMED positive code', max_length=255, required=False)
    snomed_neg = forms.CharField(label='SNOMED neg code', max_length=255, required=False)
    snomed_ind = forms.CharField(label='SNOMED indeterminate code', max_length=255, required=False)


class ConditionForm(forms.Form):
    # condition = forms.ChoiceField(choices=DiseaseDefinition.get_all_condition_choices())
    # old code
    # Condition.condition_choices(wildcard=True))
    pass


class ReferenceCaseForm(forms.Form):
    notes = forms.CharField(required=False, widget=forms.Textarea)
    ignore = forms.BooleanField(required=False)


class ConditionChoiceForm(forms.Form):
    CONDITION_CHOICES = DiseaseDefinition.get_all_test_name_search_strings() + VaersLxHeuristic.test_name_search_strings
    CONDITION_CHOICES.sort()
    CONDITION_CHOICES = [(name, name) for name in CONDITION_CHOICES]
    CONDITION_CHOICES = [(None, 'All Conditions*')] + CONDITION_CHOICES
    condition = forms.ChoiceField(label='Condition Search', choices=CONDITION_CHOICES, required=False,
                                  help_text='*Search by All Conditions may be very slow')
