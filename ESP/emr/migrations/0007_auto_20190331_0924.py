# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0006_auto_20170925_1308'),
    ]

    operations = [
        migrations.CreateModel(
            name='Providernote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('natural_key', models.CharField(help_text=b'Unique Record identifier in source EMR system', max_length=128, unique=True, null=True, blank=True)),
                ('created_timestamp', models.DateTimeField(auto_now_add=True)),
                ('updated_timestamp', models.DateTimeField(auto_now=True, db_index=True)),
                ('date', models.DateField(db_index=True)),
                ('mrn', models.CharField(max_length=50, null=True, verbose_name=b'Medical Record Number', blank=True)),
                ('encounter_natural_key', models.CharField(max_length=128, verbose_name=b'Encounter identifier in source EMR system', db_index=True)),
                ('encounter_date', models.DateField(null=True, blank=True)),
                ('type', models.CharField(max_length=200, null=True, blank=True)),
                ('purpose', models.CharField(max_length=200, null=True, blank=True)),
                ('provider_note', models.TextField()),
                ('patient', models.ForeignKey(blank=True, to='emr.Patient', null=True)),
                ('provenance', models.ForeignKey(to='emr.Provenance')),
                ('provider', models.ForeignKey(blank=True, to='emr.Provider', null=True)),
            ],
            options={
                'ordering': ['date'],
            },
        ),
        migrations.AlterModelOptions(
            name='allergy',
            options={'verbose_name_plural': 'Allergies'},
        ),
        migrations.AlterModelOptions(
            name='labinfo',
            options={'verbose_name_plural': 'Lab Info'},
        ),
        migrations.AlterModelOptions(
            name='order_idinfo',
            options={'verbose_name_plural': 'Order ID Info'},
        ),
        migrations.AlterModelOptions(
            name='patient_extradata',
            options={'verbose_name_plural': 'Patient Extra Data'},
        ),
        migrations.AlterModelOptions(
            name='pregnancy',
            options={'ordering': ['edd'], 'verbose_name_plural': 'Pregnancies'},
        ),
        migrations.AlterModelOptions(
            name='provider_idinfo',
            options={'verbose_name_plural': 'Provider ID Info'},
        ),
        migrations.AlterModelOptions(
            name='provider_phones',
            options={'verbose_name': 'Provider Phones', 'verbose_name_plural': 'Provider Phones'},
        ),
        migrations.AlterModelOptions(
            name='socialhistory',
            options={'verbose_name_plural': 'Social Histories'},
        ),
        migrations.AlterModelOptions(
            name='stiencounterextended',
            options={'verbose_name_plural': 'STI Encounter Extended'},
        ),
    ]
