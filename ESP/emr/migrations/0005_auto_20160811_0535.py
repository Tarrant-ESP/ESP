# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0004_load_initial_data'),
    ]

    operations = [
        migrations.AddField(
            model_name='stiencounterextended',
            name='pregnant_today_yn',
            field=models.CharField(max_length=50, null=True, verbose_name=b'pregnanttoday', blank=True),
        ),
        migrations.AlterField(
            model_name='stiencounterextended',
            name='hiv_test_date',
            field=models.CharField(max_length=50, null=True, verbose_name=b'hivtestdate', blank=True),
        ),
    ]
